import requests
import time
import pandas as pd
import os
import re
import yaml

from bs4 import BeautifulSoup
from unidecode import unidecode
from datetime import datetime
from datetime import date
from csv import writer
from requests.exceptions import ChunkedEncodingError
from yaml.loader import SafeLoader 

### Variables ###
RETRY_LATER_DELAY = 100
ao3URL = "https://archiveofourown.org"
ao3WorkURL = "https://archiveofourown.org/works/%s?view_adult=true&amp;view_full_work=true"
ao3SeriesURL = "https://archiveofourown.org/series/%s"
ao3LoginURL = "https://archiveofourown.org/users/login"
ao3BookmarksURL = "https://archiveofourown.org/users/%s/bookmarks"
ao3DownloadURL = "https://archiveofourown.org%s"

ao3_extensions = ["AZW3", "EPUB", "MOBI", "PDF", "HTML"]
ao3_bookmarks_status = ["public", "private", "rec"]

### Functions ###

def open_session(username, password):
    """Opens a new session and loging to ao3."""

    # Session #
    sess = requests.Session()
    
    # Getting the authenticity token
    req = sess.get(ao3URL)
    soup = BeautifulSoup(req.text, features="html.parser")
    authenticity_token = soup.find("input", {"name": "authenticity_token"})["value"]

    # Loging to ao3
    req = sess.post(ao3LoginURL, params={
            "authenticity_token": authenticity_token,
            "user[login]": username,
            "user[password]": password,
        },
    )

    # Unfortunately AO3 doesn't use HTTP status codes to communicate
    # results -- it's a 200 even if the login fails.
    if "Please try again" in req.text:
        raise RuntimeError(
            "Error logging into AO3; is your password correct?")
    
    if "Session Expired" in req.text:
        raise RuntimeError(
            "Error logging into AO3; session expired")
    
    return sess

def is_soup(workSoup):
    """Checks if workSoup is valid."""
    # Case if "retry later" page
    if workSoup.text.strip() == "Retry later":
        return False
    return True

def get_soup(sess, url, header_info=""):
    """Returns soup of the page located at url.
    Waits when there's an error and retries again (error 503)"""
    headers = {"user-agent" : header_info}
    req = sess.get(url, headers=headers)
    src = req.text
    soup = BeautifulSoup(src, "html.parser")
    # If there are too many request on the website, AO3 just show a 'retry later' page
    while not is_soup(soup):
        print("Wait....")
        time.sleep(RETRY_LATER_DELAY)
        headers = {"user-agent" : header_info}
        req = sess.get(url, headers=headers)
        src = req.text
        soup = BeautifulSoup(src, "html.parser")
    return soup

def get_lastPage(sess, url, header_info=""):
    """Returns the number of the last page from the pagination navigation."""
    
    soup = get_soup(sess, url, header_info)
    pagination_actions = (soup.find("ol", class_="pagination actions"))
    if not pagination_actions:
        lastPage = 1
    else:
        pagination = [unidecode(page.text) for page in pagination_actions]
        pagination = [page.strip() for page in pagination if page.strip()]
        lastPage = int(pagination[-2])
    return lastPage

def get_title_authors_from_li(metadata):
    """Scraps title and authors of a work from a browsing page (history, bookmarks...) and returns them."""

    heading = metadata.find("h4", class_="heading").find_all("a")
    heading = [unidecode(head.text) for head in heading]
    title = heading[0]
    authors = heading[1:]
    return title, authors

def get_rating_from_li(metadata):
    """Scraps rating of a work from a browsing page (history, bookmarks...) and returns it."""
    heading = metadata.find("ul", class_="required-tags").find_all("span", class_="text")
    symbols_list = [unidecode(head.text) for head in heading]
    rating = symbols_list[0]
    return rating

def create_folder_rec(soup, folder, file_extensions, archi_key, archi_dict):
    """Creates the folder that will contains the downloaded works and returns the path to that folder."""
    type_tag = archi_dict["type"]
    archi_tags = archi_dict["tags"]
    try:
        tag_list = [unidecode(tag.text) for tag in soup.find("dd", class_="%s tags" % type_tag).find_all(class_="tag")]
        if tag_list:
            for tag in tag_list:
            # Checking if it's in the registered tags (check Variables.py)
                if tag in archi_tags:
                    # Updating folder path
                    folder += archi_key + "/"
                    # Creating the folder if it doesn't exist
                    for file_extension in file_extensions:
                        if not os.path.exists(folder % file_extension):
                            os.mkdir(folder % file_extension)
    except AttributeError:
        return folder

    if not archi_dict["children"]:
        return folder

    children = archi_dict["children"]
    for childKey in children:
        folder = create_folder_rec(soup, folder, file_extensions, childKey, children[childKey])

    return folder

def create_folder(soup, dl_folder, file_extensions=ao3_extensions, folder_architecture=None):
    """Creates the folder that will contains the downloaded work and returns the path to that folder."""
    ## File extention
    folder = dl_folder + "%s/"
    # Creating the folder if it doesn't exist
    for file_extension in file_extensions:
        if not os.path.exists(folder % file_extension):
            os.mkdir(folder % file_extension)

    ## Case where the user doesn't wish to have the folder organized in multiples one
    if not folder_architecture:
        return folder

    ## Fandom: the works will be sorted into different folders corresponding to their fandoms
    # Retrieving fandoms
    fandoms_list = [unidecode(tag.text) for tag in soup.find("dd", class_="fandom tags").find_all(class_="tag")]
    # In case the work doesn't belong to a registered fandom (check Variables.py)
    fandoms_folder = "Other fandoms"
    # Checking if the first fandom is registered
    for fandomKey in folder_architecture.keys():
            if fandoms_list[0] in folder_architecture[fandomKey]["tags"]:
                fandoms_folder = fandomKey
                break
    # Checking if all the other fandoms are in the same group as the first (check Variables.py)
    if fandoms_folder not in ["Other fandoms"]:
        if len(fandoms_list) > 1:  
            for fandom in fandoms_list:
                # If all the fandoms aren't resgistered to be in the same group, then it's a crossover
                if fandom not in folder_architecture[fandoms_folder]["tags"]:
                    fandoms_folder = "Crossover"
                    break
    # Updating folder path
    folder += fandoms_folder + "/"
    # Creating the folder if it doesn't exist
    for file_extension in file_extensions:
        if not os.path.exists(folder % file_extension):
            os.mkdir(folder % file_extension)

    ## Other tags
    if fandoms_folder not in ["Other fandoms", "Crossover"]:
        children = folder_architecture[fandoms_folder]["children"]
        if children:
            for childKey in children:
                previous_folder = folder
                folder = create_folder_rec(soup, folder, file_extensions, childKey, children[childKey])

                # If a folder was found, no need to find another one
                # Folders in foldrs_architecture are ordered by priority (check Variables.py)
                if folder != previous_folder:
                    break

    return folder

def download_work_from_url(a_download, work_id, work_title, work_rating, folder, file_extensions=ao3_extensions):
    """Downloads the work and saves it into the corresponding folder."""

    for a in a_download:
        if a.text in file_extensions:
            while True:
                try:
                    r = requests.get(ao3DownloadURL % a["href"], stream=True)
                    r.raise_for_status()
                    title = "%s_%s_%s" % (work_rating, work_id, work_title) + "." + a.text.lower()
                    with open(folder % a.text + title[:255], 'wb') as f:
                        for chunk in r.iter_content(chunk_size=8192):
                            f.write(chunk)
                    break
                except requests.exceptions.HTTPError:
                    print("Wait...")
                    time.sleep(RETRY_LATER_DELAY)

def download_work_from_id(sess, work_id, dl_folder="", file_extensions=ao3_extensions, folder_architecture=None, header_info=""):
    """Dowloads the work from its id.
    Creates the folder that will contained the downloaded work."""

    # URL of work
    url = ao3WorkURL % work_id
    #view_adult: if it"s false then a warning appears
    #view_full_work: to get the whole work in one page instead of one by chapter

    ## Scraping
    print("Downloading work: %s" % work_id)
    # Retrieving soup
    soup = get_soup(requests, url, header_info)
    # Case if loging is necessary
    if "This work is only available to registered users of the Archive." in soup.text.strip():
        soup = get_soup(sess, url, header_info)
    # Retrieving title (for dl name)
    work_title = unidecode(soup.find("h2", class_="title heading").text).strip()
    work_title = re.sub('[^A-Za-z0-9\s]+', '', work_title)
    # Retrieving rating (for dl name)
    work_rating = soup.find("dd", class_="rating tags").find(class_="tag").text
    work_rating = unidecode(work_rating)
    work_rating = work_rating[0]

    # Retrieving download links from download button
    metadata = soup.find("ul", class_="work navigation actions")
    li_download = metadata.find("li", class_="download")
    ul_download = li_download.find("ul", "expandable secondary")
    a_download = ul_download.find_all("a", href=True)

    ## Creating the folder that will contains the downloaded fic
    folder = create_folder(soup, dl_folder, file_extensions, folder_architecture)
    ## Downloading work
    download_work_from_url(a_download, work_id, work_title, work_rating, folder, file_extensions)

def check_file_extensions(file_extensions):
    """Checks if the file extentions are valid."""

    if not file_extensions:
        raise ValueError("file_extensions cannot be empty.")
    for f_type in file_extensions:
        if f_type not in ao3_extensions:
            raise ValueError("%s isn't a valid file type." % f_type)

def check_bookmarks_status(bookmarks_status):
    """Checks if the bookmark status are valid."""

    if not bookmarks_status:
        raise ValueError("bookmarks status cannot be empty.")
    for b_status in bookmarks_status:
        if b_status not in ao3_bookmarks_status:
            raise ValueError("%s isn't a valid bookmark status." % b_status)

def get_folder_architecture(yaml_organization):
    """Loads the yaml and returns its content."""

    if yaml_organization:
        with open(yaml_organization,  encoding="utf8") as f:
            data = yaml.load(f, Loader=SafeLoader)
    else:
        data = { }
    return data

def read_series_bookmark(sess, series_id, header_info=""):
    """Returns the ids of the works in the series."""
    
    print("Downloading series: %s" % series_id)

    ## Series URL
    url = ao3SeriesURL % series_id
    
    # Retrieving number of pages
    lastPage = 0
    lastPage = get_lastPage(sess, url, header_info)

    works_id = []
    # Going through the pages of the series
    for pageIndex in range(1, lastPage + 1):
        print("\tPage: %i" % pageIndex)

        soup = get_soup(sess, url + "?page=" + str(pageIndex), header_info)
        
        works = soup.select("li.work.blurb.group")
        
        # Going through the works on the page
        for workSoup in works:

            # Retrieving id #
            group_class = workSoup.get("class")

            # Checking if the work was deleted #
            if not group_class:
                continue

            # Checking if the work wasn't deleted or hidden #
            if (workSoup.find("div", class_="mystery header picture module")) :
                continue

            # Retrieving id #
            work_id = group_class[3].split("-")[1]
            print("\tWork: %s" % work_id)

            works_id.append(work_id)

    return works_id


def read_bookmarks(username, password, dl_folder, file_extensions=ao3_extensions, bookmarks_status=ao3_bookmarks_status, yaml_organization=None, header_info=""):
    """Reads the user's bookmarks and downloads the works.
    Doesn't download works whose id are in the dl_works_csv csv file that contains already downloaded works.
    Saves new ids in the dl_works_csv csv file."""

    today = date.today()

    ## Checking file extensions
    check_file_extensions(file_extensions)
    ## Checking bookmarks status
    check_bookmarks_status(bookmarks_status)
    ## Checking folder
    if not dl_folder.endswith('/'):
        dl_folder += "/"
    if not os.path.exists(dl_folder):
        raise FileNotFoundError("This folder does not exist: %s" % dl_folder)
    
    ## Loading folder architecture
    folder_architecture = get_folder_architecture(yaml_organization)

    ## Loading id of works that were already downloaded
    previous_works = []
    # Path to new csv
    dl_works_csv = dl_folder + "works.csv"
    if os.path.exists(dl_works_csv) and os.stat(dl_works_csv).st_size > 0:
        previous_works = pd.read_csv(dl_works_csv, delimiter=";", index_col="id")
    else:
        previous_works = pd.DataFrame([], columns=["id", "title", "authors", "extensions", "date"])
        previous_works.set_index("id", inplace=True)
    
    ## Opening session and loging to ao3
    sess = open_session(username, password)

    ## Bookmarks URL
    url = ao3BookmarksURL % username
    
    # Retrieving number of pages
    lastPage = 0
    lastPage = get_lastPage(sess, url, header_info)

    # Going through the pages of the bookmarks
    for pageIndex in range(1, lastPage + 1):
        print("Page: %i" % pageIndex)

        soup = get_soup(sess, url + "?page=" + str(pageIndex), header_info)
        
        groups = soup.select("li.bookmark.blurb.group")
        
        # Going through the works on the page
        for groupSoup in groups:

            # Retrieving id #
            group_class = groupSoup.get("class")

            # Checking if the work wasn't deleted or hidden #
            if (groupSoup.find("div", class_="mystery header picture module")) :
                continue

            # Getting id #
            group_class_id = group_class[3].split("-")
            group_type, group_id = group_class_id[0], group_class_id[1]

            # Checking if the work was deleted #
            if group_type == "user":
                print("lmao this was deleted.")
                continue

            if group_type == "series":
                works_id = read_series_bookmark(sess, group_id, header_info)
            if group_type == "work":
                works_id = [group_id]

            # Retreiving type of bookmarks #
            status = groupSoup.find("p", class_="status")
            status = status.find("span")
            status = status["class"][0]

            # Retrieving date of last update
            last_update = groupSoup.find("div", class_="header module")
            last_update = last_update.find("p", class_="datetime").text
            last_update = datetime.strptime(last_update, "%d %b %Y")
            
            # Determining if this work needs to be downloaded
            will_dl = False
            extensions_to_dl = file_extensions
            if int(group_id) not in previous_works.index.to_list():
                will_dl = True
            else:
                # If the id already appears in the previous works
                previous_group = previous_works.loc[int(group_id)]

                # Checking if the work need to be reload (if updated)
                # Case 1: several occurences
                if type(previous_group).__name__ == "DataFrame":
                    for index, row in previous_group.iterrows():
                        row_date = datetime.strptime(row["date"], "%d/%m/%Y")
                        if last_update > row_date:
                            will_dl = True
                            break

                # Case 2: only one occurence
                elif type(previous_group).__name__ == "Series":
                    previous_date = datetime.strptime(previous_group["date"], "%d/%m/%Y")
                    if last_update > previous_date:
                        will_dl = True

                # Case 3: no occurence
                else:
                    will_dl = True

            # Checking extensions #
            if not will_dl:
                # Case 1: several occurences
                if type(previous_group).__name__ == "DataFrame":
                    previous_group_extension = []
                    for index, row in previous_group.iterrows():
                        previous_group_extension += row["extensions"].split(",")
                    previous_group_extension = list(set(previous_group_extension))
                # Case 2: only one occurence
                elif type(previous_group).__name__ == "Series":
                    previous_group_extension = previous_group["extensions"].split(",")
                # Case 3: no occurence
                else:
                    previous_group_extension = []

                extensions_to_dl = []
                for extension in file_extensions:
                    if extension not in previous_group_extension:
                        extensions_to_dl.append(extension)
                if extensions_to_dl:
                    will_dl = True

            if will_dl and status in bookmarks_status:
                # Downloading works
                for work_id in works_id:
                    try:
                        download_work_from_id(sess, work_id, dl_folder, extensions_to_dl, folder_architecture)
                        has_dl = True
                    except ChunkedEncodingError as e:
                        print("Error: %s couldn't be downloaded (probably because it has TOO MUCH WORDS)" % work_id)
                        has_dl = False
                        continue
                # Updating list of downloaded id
                # Getting title and authors
                title, authors = get_title_authors_from_li(groupSoup)
                rating = get_rating_from_li(groupSoup)

                if has_dl:
                    if int(group_id) not in previous_works.index.to_list():
                        # Case 1: only adding one line (new work)
                        with open(dl_works_csv, 'a', newline='') as f_dl_works:
                            writer_dl_works = writer(f_dl_works, delimiter=";")
                            if not os.stat(dl_works_csv).st_size > 0:
                                writer_dl_works.writerow(["id", "type", "rating", "title", "authors", "extensions", "date"])
                            writer_dl_works.writerow([group_id, group_type, rating, title, ",".join(authors), ",".join(file_extensions), datetime.strftime(today, "%d/%m/%Y")])
                            f_dl_works.close()
                    else:
                        # Case 2: updating one line (new extension)
                        previous_works.loc[int(group_id)]["extensions"] = ",".join(file_extensions)
                        previous_works.to_csv(dl_works_csv, mode='w', index="id", header=True, sep=";")
        pageIndex += 1