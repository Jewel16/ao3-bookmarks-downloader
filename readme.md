# bookmarks-downloader

## NEW: now download updated bookmarks!!

Hi! Here's my [AO3](https://archiveofourown.org/) bookmarks downloader. It logs into your ao3 account and browse to your bookmarks to download them.

## Dependencies

```sh
pip install -r requirements.txt
```

## How to use it

```sh
python dl_bookmarks.py --username "username" --password "password" --dl_folder "C:/Ao3 Bookmarks/" --file_extensions PDF --file_extensions EPUB --bookmarks_status public --bookmarks_status rec --bookmarks_status private --yaml_architecture "architecture.yaml"
```

#### Options

- username: your AO3 username;
- password: your AO3 password;
- dl_folder: the folder you want your bookmarks to be saved in;
- file_extensions (multiple): the extensions you want to download your bookmarks with (AZW3, EPUB, MOBI, PDF, HTML);
- bookmarks_status (multiple): the type of boomarks you want to download (public, private, rec);
- yaml_architecture (optional): file containing the architecture of the fic folder (if you want your bookmarks to be organized)

### Folder (*--dl_folder*)

This folder needs to exists, the programs won't generate it.

#### File name

Downloaded works will be saved as *rating_id_title.extension*. For example :
- *T_96524567_Holding Your Hand Through The Firnament.pdf*
- *M_78546859_anathema.epub*

File path can also not exceed 256 caracters and any special caracter will be removed.

#### *works.csv*

This folder contains (or not, but the programs will create it then) a *work.csv* file which keep track of all the fics that were downloaded. If this file exists, the programs won't download works whose id appears in it. This file gets updated after each downloaded work.

When it's a series, it's the id of the series that is stored inside the file.

|id      |type  |rating                                          |title                                                                                                                                                                                                                                                       |authors                                                               |extensions|date
|--------|------|------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------|----------|----------|
|96524567|work  |Teen And Up Audiences                           |Holding Your Hand Through The Firnament     	                                                                                                                                                                                                                                               |angst_writer                                                    |PDF,EPUB  |2023-04-04                                                        |PDF,EPUB  |2023-04-04
|86543298 |series|General Audiences, Teen And Up Audiences        |the stars are so bright tonight (but we aren't yet forgiven)                                                                                                                                                                                                                                      |angst_writer,feels_writer  |PDF,EPUB  |2023-04-04
|78546859|work  |Mature                           |anathema                                                                                                                                                                                                                                                  |summary_is_a_definition, work_is_smut                                                           |PDF,EPUB  |2023-04-04
|95674235|work  |Teen And Up Audiences                           |Miraculous Love                                                                                                                                                                                                                               |definitely_a_miraculous_crossover                                                             |PDF,EPUB  |2023-04-04
|85246795|work  |Explicit                           |Kinktober 20XX                                                                                                                                                                                                                                                 |smut_writer                                                               |PDF,EPUB  |2023-04-04

*date* is the date of when the work was downloaded.

### File extensions (*--file_extensions*)

AO3 allows users to download works in different formats: AZW3, EPUB, MOBI, PDF and HTML.

The program will create a folder for each file extension it has to download work with.

       ├── AZW3
       ├── EPUB
       ├── MOBI
       ├── PDF
       └── HTML

### Folder architecture (*--yaml_architecture*)

A yaml file can also be used to generate a folder architecture. It contains the list of fandoms (that will be the first generation of folders). Each fandom has its children, the list of its subfolders.

```yaml
BNHA:
    type: fandom
    tags:
        - 僕のヒーローアカデミア | Boku no Hero Academia | My Hero Academia
        - Pu nohiroakademia | Boku no Hero Academia | My Hero Academia
    children:
        KRBKDK:
            type: relationship
            tags: 
                - Bakugou Katsuki/Kirishima Eijirou/Midoriya Izuku
            children: 
        Vigilante Deku: 
            type: freeform
            tags: 
                - Vigilante Midoriya Izuku
            children:
                BKDK: 
                    type: relationship
                    tags: 
                        - Bakugou Katsuki/Midoriya Izuku
                        - Bakugou Katsuki/Midoriya Izuku (One-Sided)
                        - Bakugou Katsuki/Midoriya Izuku (Implied)
                    children:
                Dadzawa:
                    type: relationship
                    tags: 
                        - Parental Aizawa Shouta | Eraserhead | Dadzawa
                        - Parental Aizawa Shouta
                    children:
                    
        BKDK: 
            type: relationship
            tags: 
                - Bakugou Katsuki/Midoriya Izuku
                - Bakugou Katsuki/Midoriya Izuku (One-Sided)
                - Bakugou Katsuki/Midoriya Izuku (Implied)
            children:
MCU: 
    type: fandom
    tags: 
        - Marvel Cinematic Universe
        - The Avengers (Marvel Movies)
        - Captain America (Movies)
        - Loki (TV 2021)
    children: 
        Peter Parker's Field Trip to Stark Industries: 
            type: freeform
            tags:
                - Peter Parker's Field Trip to Stark Industries
            children: 
        Irondad & Spiderson: 
            type: relationship
            tags: 
                - Peter Parker & Tony Stark
            children:
SNK: 
    type: fandom
    tags: 
        - Shingeki no Kyojin | Attack on Titan    
    children:
```

       └── EPUB
           ├── BNHA 
           │   ├── KRBKDK
           │   ├── Vigilante Deku
           │   │   └── Dadzawa
           │   └── BKDK
           ├── MCU   
           │   ├── Irondad & Spiderson
           │   └── Peter Parker's Fieldtrip To Stark Industries
           └── SNK

Works will go to the first tag of the yaml file.

#### Examples

- A work tagged with *Bakugou Katsuki/Midoriya Izuku* first, then *Bakugou Katsuki/Kirishima Eijirou/Midoriya Izuku* will go in the *KRBKDK* folder because in the yaml, *KRBKDK* appears first.
- A work tagged with *Parental Aizawa Shouta* and *Vigilante Midoriya Izuku* will go to the *Dadzawa* subfolder of the *Vigilante Deku* folder because *Vigilante Deku* folder appears first in the yaml file and also has *Dadzawa* in its children.
- A work tagged with *Peter Parker & Tony Stark* and *Peter Parker's Fieldtrip To Stark Induestries* will go the *Peter Parker's Fieldtrip To Stark Induestries* folder since this tag appears first in the yaml file.
- A work tagged *Bakugou Katsuki/Midoriya Izuku (Implied)* will go into the *BKDK* folder because it appears in the tag list of this folder in the yaml file, but a work tagged with *Bakugou Katsuki/Midoriya Izuku (Background)* won't because the tag doesn't appear in the yaml file.

## Ao3 requests

This program makes requests to Ao3:
- `"https:/archivesofourown.org/"`: to log into the website;
- `"https://archiveofourown.org/users/login"`: to browse the user's bookmarks;
- `"https://archiveofourown.org/series/%s" % series_id`: to browse works of a series;
- `"https://archiveofourown.org/works/%s?view_adult=true&amp;view_full_work=true" % work_id`: to get to the download link of the work;
- `"https://archiveofourown.org/series/%s" % download_link`: to download the work.

The session returned by the logging is used to make the requests to the bookmarks browsing pages.

When sending requests to works and downloading links, a simple `requests` is used. This way, those works won't go up in the user's history. The only exception is for locked works, since it's only accessible to registered users.

## Errors

When a work is deleted, a message appears in the and the programs resumes.

When a work is too big (`ChunkedEncodingError`) for the request, a message appears to warn the user and the programs resumes.

## Licence

This work is licenced under the CC BY-NC-SA.

___
> **CC BY-NC-SA:** This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format for noncommercial purposes only, and only so long as attribution is given to the creator. If you remix, adapt, or build upon the material, you must license the modified material under identical terms. ([CreativeCommons](https://creativecommons.org/about/cclicenses/))
___

## Credits

As I didn't know how to scrap before, I took inspiration from this [AO3 Scraper](https://github.com/radiolarian/AO3Scraper) by [radiolarian](https://github.com/radiolarian). Check their work out!
